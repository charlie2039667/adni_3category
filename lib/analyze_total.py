# this file analyze and output results of all testing data
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import metrics

# drawing ROC curve of target class
def DrawROC(config, title, prob, true, target_class):
    encode_prob = [row[target_class].tolist() for row in prob]
    encode_true = list(map(lambda i: 1 if i == target_class else 0, true))
    
    if not (1 in encode_true):
        print('No positive samples in labels')
        return 0
    
    if not (0 in encode_true):
        print('No negative samples in labels')
        return 0
    
    auc = metrics.roc_auc_score(encode_true, encode_prob)
    
    if not os.path.exists(os.path.join(config.model_save_dir, 'ROC')):
            os.mkdir(os.path.join(config.model_save_dir, 'ROC'))
            
    fpr, tpr, _ = metrics.roc_curve(encode_true,  encode_prob)
    plt.plot(fpr, tpr, label = 'Prediction')
    plt.plot([0, 1], [0, 1], '--b', label = 'Random Guess')
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    plt.savefig(os.path.join(config.model_save_dir + '/ROC', title + 'ROC.png'))
    plt.show()
                             
    return auc

# count accuracy, precision, recall and f1 score of target class
def CountAPRF(config, title, prob, pred, true, target_class, plot = False):
    TP = 0
    FP = 0
    TN = 0
    FN = 0
    
    confusion = np.zeros((3, 3))
    for i in range(len(true)):
        confusion[true[i]][pred[i]] += 1

    if plot:
        print("Confision matrix:")
        for i in range(3): 
            print(confusion[i, :])
        
        if not os.path.exists(os.path.join(config.model_save_dir, 'Confusion')):
            os.mkdir(os.path.join(config.model_save_dir, 'Confusion'))
            
        np.savetxt(os.path.join(config.model_save_dir + '/Confusion', title[:-3] + 'Confusion.txt'), confusion, delimiter=' ', fmt='%d')
    
    for label in range(len(confusion)):
        for predict in range(len(confusion[label])):
            if label == target_class:
                if predict == target_class:
                    TP += confusion[label][predict]
                else:
                    FN += confusion[label][predict]
            else:
                if predict == target_class:
                    FP += confusion[label][predict]
                else:
                    TN += confusion[label][predict]
    
    accu = (TP + TN) / (TP + TN + FP + FN)
    
    if TP + FP + FN == 0:
        precision = 0
        recall = 0
        f1 = 0
    
    else:
        if TP + FP == 0:
            precision = 0
            recall = TP / (TP + FN)
            
        elif TP + FN == 0:
            precision = TP / (TP + FP)
            recall = 0

        else:
            precision = TP / (TP + FP)
            recall = TP / (TP + FN)
            
        if precision + recall == 0:
            f1 = 0
            
        else:
            f1 = 2 * precision * recall / (precision + recall)
        
    
    # Draw ROC curve
    auc = DrawROC(config, title, prob, true, target_class)
    print("auc", auc)
    
    return accu, precision, recall, f1, auc             

# process all analyzing steps of all testing data and output result to csv/txt files
def TotalAnalyze(config, y_test_prob, y_test_pred, y_test_true,
                best_train_accu, best_accu, test_accuracy):

    ca, cp, cr, cf, cau = CountAPRF(config, 'Total_CN_', y_test_prob, y_test_pred, y_test_true, 0, plot = True)
    print("CN Accuracy:{:.4f}, Precision:{:.4f}, Recall:{:.4f}, F1 Score:{:.4f}".format(ca, cp, cr, cf))
    ma, mp, mr, mf, mau = CountAPRF(config, 'Total_MCI_', y_test_prob, y_test_pred, y_test_true, 1)
    print("MCI Accuracy:{:.4f}, Precision:{:.4f}, Recall:{:.4f}, F1 Score:{:.4f}".format(ma, mp, mr, mf))
    aa, ap, ar, af, aau = CountAPRF(config, 'Total_AD_', y_test_prob, y_test_pred, y_test_true, 2)
    print("AD Accuracy:{:.4f}, Precision:{:.4f}, Recall:{:.4f}, F1 Score:{:.4f}".format(aa, ap, ar, af))

    total_list = [best_train_accu, best_accu, test_accuracy, ca, cp, cr, cf, cau, ma, mp, mr, mf, mau, aa, ap, ar, af, aau]
    total_df = pd.DataFrame([total_list], columns = ['train accu', 'valid accu',
    'test_accu', 'CN accu', 'CN precision', 'CN recall', 'CN f1 score', 'CN AUC', 
    'MCI accu', 'MCI precision', 'MCI recall', 'MCI f1 score', 'MCI AUC', 'AD accu',
    'AD precision', 'AD recall', 'AD f1 score', 'AD AUC'])

    if not os.path.exists(os.path.join(config.model_save_dir, 'Analy')):
                os.mkdir(os.path.join(config.model_save_dir, 'Analy'))
            
    total_df.to_csv(config.model_save_dir + '/Analy/Total_Analy.csv', index=False)