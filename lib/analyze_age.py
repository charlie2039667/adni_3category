# this file analyze and output results of target age of testing data
import numpy as np
import math
import pandas as pd
from tqdm import tqdm
from .analyze_total import CountAPRF
from sklearn.metrics import accuracy_score

# generate list of sex of testing data for analyzation 
def GenerateAgeList(df, x_test):    
    age_list = []

    for file_ID in tqdm(x_test):
        target = file_ID.split('_')[-1].split('.')[0]
        
        for i in range(len(df)):
            if df['Image Data ID'][i] == target:
                age_list.append(df['Age'][i])
                break

    # count number of each margin
    age_margin = np.zeros(5)

    for age in age_list:
        num = math.ceil(int(age) / 10) - 6
        age_margin[num] += 1
        
    print("Age margin:", age_margin)
    print("Age max:", max(age_list))
    print("Age min:", min(age_list))

    return age_list

def AgeAnaly(config, age_output, age_list, y_test_true, y_test_pred, y_test_prob):
    for idx in range(5):
        m_pred = []
        m_true = []
        m_prob = []
        ceil = (idx+6)*10
        floor = (idx+5)*10+1
        print("Age {} ~ {} :".format(floor, ceil))
        outstr = str(floor) + "~" + str(ceil)
        
        for i in range(len(age_list)):
            age = math.ceil(int(age_list[i]) / 10) - 6
            if age == idx:
                m_pred.append(y_test_pred[i])
                m_true.append(y_test_true[i])
                m_prob.append(y_test_prob[i])
                
        accu = accuracy_score(m_true, m_pred)
        
        print("Data Size:", len(m_pred))
        print("Total accu:", accu)

        ca, cp, cr, cf, cau = CountAPRF(config, str(floor) + 'to' + str(ceil) + '_CN_', m_prob, m_pred, m_true, 0, plot = True)
        print("CN Accuracy:{:.4f}, Precision:{:.4f}, Recall:{:.4f}, F1 Score:{:.4f}".format(ca, cp, cr, cf))
        print("---------------------------------------")
        ma, mp, mr, mf, mau = CountAPRF(config, str(floor) + 'to' + str(ceil) + '_MCI_', m_prob, m_pred, m_true, 1)
        print("MCI Accuracy:{:.4f}, Precision:{:.4f}, Recall:{:.4f}, F1 Score:{:.4f}".format(ma, mp, mr, mf))
        print("---------------------------------------")
        aa, ap, ar, af, aau = CountAPRF(config, str(floor) + 'to' + str(ceil) + '_AD_', m_prob, m_pred, m_true, 2)
        print("AD Accuracy:{:.4f}, Precision:{:.4f}, Recall:{:.4f}, F1 Score:{:.4f}".format(aa, ap, ar, af))
        print("---------------------------------------")
        print("\n")
        
        row = [outstr, len(m_pred), accu, ca, cp, cr, cf, cau, ma, mp, mr, mf, mau, aa, ap, ar, af, aau]
        age_output.append(row)

    return age_output

def AgeAnalyze(config, x_test, y_test_true, y_test_pred, y_test_prob):
    print("Generate list of age...")
    age_output = []
    age_list = GenerateAgeList(config.mri_df, x_test)

    age_output = AgeAnaly(config, age_output, age_list, y_test_true, y_test_pred, y_test_prob)

    age_df = pd.DataFrame(data = age_output)
    age_df.columns = ['Age', 'Data Number', 'Total Accuracy', 'CN Accuracy', 'CN Precision', 'CN Recall', 'CN F1 Score', 'CN AUC', 'MCI Accuracy', 'MCI Precision', 'MCI Recall', 'MCI F1 Score', 'MCI AUC', 'AD Accuracy', 'AD Precision', 'AD Recall', 'AD F1 Score', 'AD AUC']
    age_df.to_csv(config.model_save_dir + '/Analy/Age_Analy.csv', index=False)