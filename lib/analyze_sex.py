# this file analyze and output results of target sex of testing data
import numpy as np
import math
import pandas as pd
from tqdm import tqdm
from .analyze_total import CountAPRF
from sklearn.metrics import accuracy_score

# generate list of sex of testing data for analyzation 
def GenerateSexList(df, x_test):
    sex_list = []

    for file_ID in tqdm(x_test):
        target = file_ID.split('_')[-1].split('.')[0]
        
        for i in range(len(df)):
            if df['Image Data ID'][i] == target:
                sex_list.append(df['Sex'][i])
                break

    # count number of each margin
    sex_margin = np.zeros(2)
        
    for sex in sex_list:
        if sex == 'M':
            sex_margin[0] += 1
        elif sex == 'F':
            sex_margin[1] += 1
    
    print("Sex margin:", sex_margin)

    return sex_list

# analyzation of target sex
def SexAnaly(config, sex, sex_list, sex_output, y_test_true, y_test_pred, y_test_prob):
    m_pred = []
    m_true = []
    m_prob = []

    for i in range(len(sex_list)):
        if sex_list[i] == sex:
            m_pred.append(y_test_pred[i])
            m_true.append(y_test_true[i])
            m_prob.append(y_test_prob[i])
            
    accu = accuracy_score(m_true, m_pred)

    print("Data Size:", len(m_pred))
    print("Total accu:", accu)

    ca, cp, cr, cf, cau = CountAPRF(config, sex + '_CN_', m_prob, m_pred, m_true, 0, plot = True)
    print("CN Accuracy:{:.4f}, Precision:{:.4f}, Recall:{:.4f}, F1 Score:{:.4f}".format(ca, cp, cr, cf))
    print("---------------------------------------")
    ma, mp, mr, mf, mau = CountAPRF(config, sex + '_MCI_', m_prob, m_pred, m_true, 1)
    print("MCI Accuracy:{:.4f}, Precision:{:.4f}, Recall:{:.4f}, F1 Score:{:.4f}".format(ma, mp, mr, mf))
    print("---------------------------------------")
    aa, ap, ar, af, aau = CountAPRF(config, sex + '_AD_', m_prob, m_pred, m_true, 2)
    print("AD Accuracy:{:.4f}, Precision:{:.4f}, Recall:{:.4f}, F1 Score:{:.4f}".format(aa, ap, ar, af))
    print("---------------------------------------")
    print('\n')
    
    row = [sex, len(m_pred), accu, ca, cp, cr, cf, cau, ma, mp, mr, mf, mau, aa, ap, ar, af, aau]
    sex_output.append(row)

    return sex_output

# process all analyzing steps of each sex of all testing data and output result to csv/txt files
def SexAnalyze(config, x_test, y_test_true, y_test_pred, y_test_prob):
    print("Generate list of sex...")
    sex_output = []
    sex_list = GenerateSexList(config.mri_df, x_test)

    print("Sex M:")
    sex_output = SexAnaly(config, 'M', sex_list, sex_output, y_test_true, y_test_pred, y_test_prob)

    print("Sex F:")
    sex_output = SexAnaly(config, 'F', sex_list, sex_output, y_test_true, y_test_pred, y_test_prob)

    sex_df = pd.DataFrame(data = sex_output)
    sex_df.columns = ['Sex', 'Data Number', 'Total Accuracy', 'CN Accuracy', 'CN Precision', 'CN Recall', 'CN F1 Score', 'CN AUC', 'MCI Accuracy', 'MCI Precision', 'MCI Recall', 'MCI F1 Score', 'MCI AUC', 'AD Accuracy', 'AD Precision', 'AD Recall', 'AD F1 Score', 'AD AUC']
    sex_df.to_csv(config.model_save_dir + '/Analy/Sex_Analy.csv', index=False)