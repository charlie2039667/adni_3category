# this file defines the model of ADNI training
import torch.nn as nn

# define CNN Model
class CNNModel(nn.Module):
    def __init__(self):
        super(CNNModel, self).__init__()
        
        self.conv_layer1 = self._conv_layer_set(1, 16)
        self.conv_layer2 = self._conv_layer_set(16, 32)
        self.conv_layer3 = self._conv_layer_set(32, 64)
        self.conv_layer4 = self._conv_layer_set(64, 128)
        self.conv_layer5 = self._conv_layer_set(128, 256)
        
        self.fc1 = nn.Linear(256*3*3*2, 40)
        self.relu = nn.LeakyReLU()
        
        self.fc2 = nn.Linear(40, 3)
        self.tanh = nn.Tanh()
        self.softmax = nn.Softmax(dim=1)
        
        #self.batch=nn.BatchNorm1d(128)
        self.drop=nn.Dropout(p=0.3)
        
        
    def _conv_layer_set(self, in_c, out_c):
        conv_layer = nn.Sequential(
            nn.Conv3d(in_c, out_c, kernel_size=(3, 3, 3), padding=1),
            nn.BatchNorm3d(out_c),
            nn.Tanh(),
            nn.MaxPool3d(kernel_size=(2, 2, 2)),
        )
        return conv_layer
    

    def forward(self, x):
        x = x.float()
        
        out = self.conv_layer1(x)
        out = self.conv_layer2(out)
        out = self.conv_layer3(out)
        out = self.conv_layer4(out)
        out = self.conv_layer5(out)
        #print(out.shape)
        
        out = out.view(out.size(0), -1)
        
        out = self.fc1(out)
        out = self.drop(out)
        #out = self.relu(out)
        
        out = self.fc2(out)
        #out = self.relu2(out)
        
        prob = self.softmax(out)
        
        return out, prob