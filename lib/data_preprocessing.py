# this file deals with data preprocessing, including merge ehr and mri
# , split data, and preprocess training data

from .global_config import Data
from tqdm import tqdm
import random
import numpy as np

# create data slice
def CreateData(ehr_df, datapath):
    RIDlist = ehr_df['RID']
    feature_df = ehr_df.drop(columns = 'RID')
    data_slice = []

    for filename in datapath:
        RID = GetRID(filename)

        idx = RIDlist.loc[lambda x: x==RID].index.item()
        features = feature_df.loc[[idx]]
        data = Data(filename, features)
        data_slice.append(data)
    
    return data_slice

# get rid from filename of mri
def GetRID(filename):
    rid = int(filename.split('_')[3])
    return rid

# split data that follow this rule -- all data of same 
# patient should be in the same data
def SplitData(datapath, label, mri_df, ratio):
    patient_list = GetPatient(datapath, mri_df)
    patient_split_ID = SplitPatientID(patient_list, ratio, datapath)
    
    x_train_inba, y_train_inba = SplitPatientData(patient_split_ID[0], datapath, label)
    x_valid, y_valid = SplitPatientData(patient_split_ID[1], datapath, label)
    x_test, y_test = SplitPatientData(patient_split_ID[2], datapath, label)

    return x_train_inba, y_train_inba, x_valid, y_valid, x_test, y_test

    
# get list of patient RID with occurrence
# ex. ['002_S_0295', 6]
def GetPatient(datapath, df):
    patient_list = []

    print("Get Patient List...")
    for idx in tqdm(range(len(datapath))):
        imgID = datapath[idx].split('\\')[-1].split('_')[-1].split('.')[0]
        
        target = ''
        for i in range(len(df['Image Data ID'])):
            if df['Image Data ID'][i] == imgID:
                target = df['Subject'][i]
                break
        
        flag = False
        for i in range(len(patient_list)):
            if patient_list[i][0] == target:
                patient_list[i][1] += 1
                flag = True
                break
                
        if not flag:
            row = [target, 1]
            patient_list.append(row)

    return patient_list

# split patient id to training, validation and testing with 
# balanced total occurence of each data
def SplitPatientID(patient_list, ratio, datapath):
    train_patient = []
    valid_patient = []
    test_patient = []
    train_ckpt = 0
    valid_ckpt = 0
    count = 0
    phase = 0
    train_ratio, valid_ratio = ratio[0], ratio[1]

    #random pick patient to train, validation and test
    random_split = random.sample(range(len(patient_list)), len(patient_list))

    for idx in random_split:
        if phase == 0:
            if count >= len(datapath) * train_ratio:
                phase += 1
                train_ckpt = count
                valid_patient.append(patient_list[idx][0])
                count += patient_list[idx][1]
                print("Train:", train_ckpt)
            else:
                train_patient.append(patient_list[idx][0])
                count += patient_list[idx][1]
        elif phase == 1:
            if count >= len(datapath) * (train_ratio + valid_ratio):
                phase += 1
                valid_ckpt = count
                test_patient.append(patient_list[idx][0])
                print("Valid:", valid_ckpt - train_ckpt)
            else:
                valid_patient.append(patient_list[idx][0])
                count += patient_list[idx][1]
        else:
            test_patient.append(patient_list[idx][0])

    print("Test:", len(datapath) - valid_ckpt)

    return [train_patient, valid_patient, test_patient]

# split each mri data according to patient_split_id list
def SplitPatientData(split_id, datapath, label):
    x = []
    y = []
    
    for patient in split_id:
        for idx in range(len(datapath)):
            patientID = datapath[idx].split('\\')[-1].split('_')[:3]
            patientID = '_'.join(patientID)
            if patientID == patient:
                x.append(datapath[idx])
                y.append(label[idx])
                
    return x, y

# count number of each label of label list
def CountData(countlist):
    count = np.zeros(3)

    for i in countlist:
        count[i] += 1
        
    print("Training labels count:", count)

# deal with data balance of training data
def DataBalance(x_train_inba, y_train_inba):
    CNlist = []
    MCIlist = []
    ADlist = []

    CountData(y_train_inba)

    for i in range(len(x_train_inba)):
        if y_train_inba[i] == 0:
            CNlist.append(x_train_inba[i])
        elif y_train_inba[i] == 1:
            MCIlist.append(x_train_inba[i])
        elif y_train_inba[i] == 2:
            ADlist.append(x_train_inba[i])

    target_num = max(len(CNlist), len(MCIlist), len(ADlist))
    
    CNtimes = round(target_num / len(CNlist))
    MCItimes = round(target_num / len(MCIlist))
    ADtimes = round(target_num / len(ADlist))

    print("CN times:{}, MCI times:{}, AD times:{}".format(CNtimes, MCItimes, ADtimes))
    print("New data: CN:{}, MCI:{}, AD:{}".format(CNtimes*len(CNlist), MCItimes*len(MCIlist), ADtimes*len(ADlist)))

    x_train = x_train_inba.copy()
    y_train = y_train_inba.copy()

    for i in range(CNtimes-1):
        x_train += CNlist
        label_list = [0] * len(CNlist)
        y_train += label_list
        
    for i in range(MCItimes-1):
        x_train += MCIlist
        label_list = [1] * len(MCIlist)
        y_train += label_list

    for i in range(ADtimes-1):
        x_train += ADlist
        label_list = [2] * len(ADlist)
        y_train += label_list 

    print("Number of original data:", len(x_train_inba))
    print("Number of original label:", len(y_train_inba))
    print("Number of balanced data:", len(x_train))
    print("Number of balanced label:", len(y_train))

    return x_train, y_train

