# this file deals with training process of ADNI
import torch
import os
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
from sklearn.metrics import accuracy_score

# define training process
def train(config, device, model, pretrained, pretrained_path,
            train_dataloader, valid_dataloader, optimizer, error):
    if pretrained:
        model.load_state_dict(torch.load(pretrained_path))
        print("Loading pretrained model")
    
    model.train()

    # Model training

    train_accu = []
    valid_accu = []

    best_epoch = -1
    best_accu = 0
    best_train_epoch = -1
    best_train_accu = 0
    ckpt_path = os.path.join(config.model_save_dir, "best.pt")

    for epoch in range(config.num_epochs):
        y_train_pred = []
        y_train_true = []
        y_valid_pred = []
        y_valid_true = []
        train_loss = 0
        
        print("### Epoch {} ###".format(epoch))
        for idx, (_, image, label) in tqdm(enumerate(train_dataloader)):
            
            image = image.to(device)
            label = label.to(device)
            
            # Clear gradients
            optimizer.zero_grad()
            
            # Forward propagation
            output, _ = model(image)
    #         print(output)
    #         print(label)
            
            # Calculate loss
            loss = error(output, label)
            train_loss += loss
            
            # Calculating gradients
            loss.backward()
            
            # Update parameters
            optimizer.step()
            
            train_output = output.cpu().detach().numpy()
            train_label = label.cpu().detach().numpy()
            
            for i in range(len(train_output)):
                y_train_pred.append(np.argmax(train_output[i]))
                y_train_true.append(train_label[i])

        train_accuracy = accuracy_score(y_train_true, y_train_pred)

        if train_accuracy > best_train_accu:
            best_train_epoch = epoch
            best_train_accu = train_accuracy
            

        # Iterate through valid dataset
        for valid_idx, (_, valid_image, valid_label) in tqdm(enumerate(valid_dataloader)):
            
            valid_image = valid_image.to(device)
            valid_label = valid_label.to(device)

            valid_output, _ = model(valid_image)

            valid_output = valid_output.cpu().detach().numpy()
            valid_label = valid_label.cpu().detach().numpy()
            
            for i in range(len(valid_output)):
                y_valid_pred.append(np.argmax(valid_output[i]))
                y_valid_true.append(valid_label[i])

        valid_accuracy = accuracy_score(y_valid_true, y_valid_pred)
        
        if valid_accuracy > best_accu:
            best_epoch = epoch
            best_accu = valid_accuracy
            torch.save(model.state_dict(), ckpt_path)
            print("Store best model for epoch", epoch)

        # store accuracy and iteration
        
        train_accu.append(train_accuracy)
        valid_accu.append(valid_accuracy)

        # Print Loss
        print("Epoch: {}  Training Loss: {}, Training Accuracy: {}, Validation Accuracy {}".format(epoch, train_loss, train_accuracy, valid_accuracy))
    
    print("Best Train Epoch:", best_train_epoch)
    print("Best Train Accuracy:", best_train_accu)
    print("Best Valid Epoch:", best_epoch)
    print("Best Valid Accuracy:", best_accu)

    PlotHistory(config, train_accu, valid_accu)

    return best_train_accu, best_accu


def PlotHistory(config, train_accu, valid_accu):
    epoch_list = list(range(config.num_epochs))

    plt.plot(epoch_list, train_accu, label = "Train Accuracy")
    plt.plot(epoch_list, valid_accu, label = "Validation Accuracy")
    plt.savefig(config.model_save_dir + '/training_curve.png')
    plt.legend()
    plt.show()