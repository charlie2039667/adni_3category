# this file deals with inference
import os
import torch
import numpy as np
import pandas as pd
from tqdm import tqdm
from sklearn.metrics import accuracy_score

# deals with inference, and return probability vector and prediction of testing data
def inference(config, model, device, dataloader):
    y_test_pred = []
    y_test_true = []
    y_test_prob = []

    model.load_state_dict(torch.load(config.ckpt_path))
    model.eval()

    with torch.no_grad():
        for test_idx, (_, test_image, test_label) in tqdm(enumerate(dataloader)):

            test_image = test_image.to(device)
            test_label = test_label.to(device)

            test_output, test_prob = model(test_image)
            #print(test_output)

            test_output = test_output.cpu().detach().numpy()
            test_label = test_label.cpu().detach().numpy()

            for i in range(len(test_output)):
                y_test_prob.append(test_prob[i])
                y_test_pred.append(np.argmax(test_output[i]))
                y_test_true.append(test_label[i])

    test_accuracy = accuracy_score(y_test_true, y_test_pred)
    print("Test Accuracy:", test_accuracy)

    return test_accuracy, y_test_true, y_test_prob, y_test_pred

# output probability vector tof testing data to csv
# pick the max probability of label of same patient among his/her mri images
def ProbabilityToCsv(config, x_test, y_test_true, y_test_prob, y_test_pred):
    
    output_csv = []
    prev_id = ''
    best_row = []
    best_prob = 0

    for i in range(len(x_test)):
        id_str = x_test[i].split('_')[3]
        f_id = str(int(id_str))
        row = [f_id, y_test_true[i], y_test_pred[i]]
        row += y_test_prob[i].tolist()
        
        if not f_id == prev_id:
            if not i == 0:
                output_csv.append(best_row)
                
            best_row = row.copy()
            label = row[1]
            best_prob = row[label+2]
            prev_id = f_id
            
            
            
        else:
            label = row[1]
            prob = row[label+2]
            
            if prob > best_prob:
                best_prob = prob
                best_row = row.copy()

    out_df = pd.DataFrame(output_csv, columns= ['RID', 'Group', 'Prediction', 'CN prob', 'MCI prob', 'AD prob'])
    out_df.to_csv(os.path.join(config.model_save_dir, config.mri_prob_path), index=False)
    print("Inference to csv done")