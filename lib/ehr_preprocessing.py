# this file deals with preprocessing of ehr data

import pandas as pd
import numpy as np
from sklearn import preprocessing

# do all steps of ehr preprocessing
def Preprocessing(ehr_root, manual_feature, sel_col):
    prun_df = PrunMissingData(ehr_root)
    fill_df = FillNAByMean(prun_df)
    encode_df = EncodeEHR(fill_df)
    
    if manual_feature:
        sel_df = ChooseFeatures(encode_df, sel_col)
        return sel_df
    else:
        return encode_df
        


# prun features of missing rate > 0.5
def PrunMissingData(ehr_root):
    ehr_raw_df = pd.read_csv(ehr_root, encoding = 'utf-8')
    size = len(ehr_raw_df['RID'])
    drop_list = []

    for col in ehr_raw_df.columns:
        if ehr_raw_df[col].isnull().sum() >= size / 2:
            drop_list.append(col)

    prun_df = ehr_raw_df.drop(columns = drop_list)

    return prun_df

# fill NA(missing data) by mean of that feature
def FillNAByMean(prun_df):
    na_col = prun_df.columns[prun_df.isna().any()].tolist()
    fill_df = prun_df.copy()

    for col in na_col:
        m = prun_df[col].mean()
        fill_df[col].fillna(value=m, inplace=True)
        
    return fill_df

# Encode EHR by label encoder
def EncodeEHR(fill_df):
    encode_features = [col for col, dt in fill_df.dtypes.items() if dt == object]
    encode_df = fill_df.copy()

    for col in encode_features:
        le = preprocessing.LabelEncoder()
        le.fit(encode_df[col])
        encode_df[col] = le.transform(encode_df[col])

    return encode_df

# Manual choose features if needed
def ChooseFeatures(encode_df, sel_col):
    sel_col = sel_col + ['Label', 'RID']
    return encode_df[sel_col]

