# this file defines dataset and dataloader

import numpy as np
import random
import os
import torchvision.transforms as transforms
from torch.utils.data import Dataset, DataLoader
from skimage import exposure, util
from skimage.transform import resize
from scipy.ndimage import rotate

# dataset for MRI + EHR
class ADNIDataset(Dataset):
    def __init__(self, config, pathlist, label, phase):
        (self.width, self.height, self.depth) = config.shape
        self.pathlist = pathlist
        self.label = label
        self.phase = phase
        self.config = config
        
    def __len__(self):
        return len(self.pathlist)
    
    def __getitem__(self, idx):
        # load npy file
        image = self.load_npy(os.path.join(self.config.data_dir, self.pathlist[idx]))
        
        # resize image, deprecated because of time consuming
        # directly resize it in npy
        # image = self.resize(image, size=(self.width, self.height, self.depth))
        
        # random augmentation for training data
        if self.phase == "train":
            image = self.randomAug(image)
        
        # normalize
        image = self.normalize(image)
        
        # save original image and do histogram equalization
        ori_image = image
        image = self.histo_equal(image)
        
        # add one color channel
        image = np.array([image])
        ori_image = np.array([ori_image])
        
        # get label
        label = self.label[idx]
        
        return ori_image, image, label
    
    def randomAug(self, img):
        # random rotate
        degree = random.randint(0, 15)
        img = rotate(img, angle = degree, order=1, reshape=False, axes=(0,1))
        
        return img
        
    def histo_equal(self, im_orig):
        
        # Degrade image by applying exponential intensity decay along x
        sigmoid = np.exp(-3 * np.linspace(0, 1, im_orig.shape[0]))
        im_degraded = (im_orig.T * sigmoid).T

        # Set parameters for AHE
        # Determine kernel sizes in each dim relative to image shape
        kernel_size = (im_orig.shape[0] // 5,
                       im_orig.shape[1] // 5,
                       im_orig.shape[2] // 2)
        kernel_size = np.array(kernel_size)
        clip_limit = 0.9

        # Perform histogram equalization
        im_orig_he, im_degraded_he = \
            [exposure.equalize_hist(im)
             for im in [im_orig, im_degraded]]

        im_orig_ahe, im_degraded_ahe = \
            [exposure.equalize_adapthist(im,
                                         kernel_size=kernel_size,
                                         clip_limit=clip_limit)
             for im in [im_orig, im_degraded]]
        
        return im_orig_he
    
    def load_npy(self, file_path):
        image = np.load(file_path)
        return image
    
    def normalize(self, data: np.ndarray):
        data_min = np.min(data)
        return (data - data_min) / (np.max(data) - data_min)
    
    def resize(self, data: np.ndarray, size):
        data = resize(data, size, preserve_range=True)
        return data

# dataloader
def GetDataloader(dataset, config, pathlist, label, phase):
    dataset = dataset(config, pathlist, label, phase)
    dataloader = DataLoader(
        dataset,
        batch_size = config.batch_size,
        num_workers = config.num_workers,
        pin_memory=True,
        shuffle=True,
        drop_last=False)
    
    return dataloader