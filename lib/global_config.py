# this file defines global config and data slice structure

from asyncio.windows_events import NULL
import os
import torch.nn as nn

# basic definition of global parameters
class GlobalConfig():
    ## parameters in this part are predefined
    # data directory
    data_dir = "./data"

    # directory for data generated during training
    model_save_dir = "./model"

    # path of text file of mri data list 
    datapath_list = "datapath_list.txt"

    # path of text file of label list, sorted as datapath_list 
    label_list = "label_list.txt"

    # path of checkpoint of best validation accuracy 
    ckpt_path = "./model/best.pt"

    # path of save the csv of testing data probability vector
    mri_prob_path = 'MRI_prob.csv'

    # mri data directory
    data_root = os.path.join(data_dir, "Npy_Dataset")

    # mri csv filename
    csv_root = os.path.join(data_dir, "MP__1_13_2022.csv")

    # ehr csv filename
    ehr_root = os.path.join(data_dir, "Merge_features_v2.csv")
    
    # learning rate
    lr = 0.001

    # training epochs
    num_epochs = 50

    # batch size
    batch_size = 8

    # number of workers for dataloader
    num_workers = 0

    # data size for training
    shape = (96, 96, 80) # origin: (192, 192, 160)

    ## parameters in this part are construct by constructing function
    # list of mri datapath
    datapath = []

    # list of labels, sort as list of mri datapath
    label = []
    
    ## parameters in this part are passed by main function
    # dataframe of ehr
    ehr_df = []

    # dataframe of mri
    mri_df = []
    
    # error history during training
    train_error = []
    valid_error = []
    
    # data slice of merging ehr features and mri datapath
    # type: [Data, Data, ...]
    data_slice = []

    def __init__(self):
        datapath_list = open(os.path.join(self.data_dir, self.datapath_list), 'r')
        self.datapath = datapath_list.read().split('\n')
        self.datapath = self.datapath[:-1]

            
        label_list = open(os.path.join(self.data_dir, self.label_list), 'r')
        self.label = label_list.read().split('\n')
        self.label = self.label[:-1]
        label_map = map(int, self.label)
        self.label = list(label_map)

        print(len(self.datapath))
        print(len(self.label))
    
class Data():
    datapath = ""
    features = []
    
    def __init__(self, path, feat):
        self.datapath = path
        self.features = feat